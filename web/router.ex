defmodule MebeWeb.Router do
  use Phoenix.Router

  pipeline :browser do
    plug MebeWeb.RequestStartTimePlug
    plug :accepts, ["html", "xml"]
    plug :fetch_session
    plug :fetch_flash
    plug :put_secure_browser_headers
  end

  scope "/", MebeWeb do
    pipe_through :browser

    get "/p/:page", PageController, :index
    get "/", PageController, :index
    get "/feed", FeedController, :index

    get "/tag/:tag/p/:page", PageController, :tag
    get "/tag/:tag", PageController, :tag
    get "/tag/:tag/feed", FeedController, :tag

    get "/author/:author/p/:page", PageController, :author
    get "/author/:author", PageController, :author
    get "/author/:author/feed", FeedController, :author

    get "/archive/:year/p/:page", PageController, :year
    get "/archive/:year", PageController, :year
    get "/archive/:year/:month/p/:page", PageController, :month
    get "/archive/:year/:month", PageController, :month

    get "/:year/:month/:day/:slug", PageController, :post
    get "/:slug", PageController, :page
  end
end
