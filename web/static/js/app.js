import checkForLaineHash from './old_hash_redirector';
import './syntaxhighlight';

// Check hash even before site is loaded since it doesn't need to
// wait for loading
checkForLaineHash();

window.onload = () => {
};
