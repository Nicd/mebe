/*
 * This script will check old Laine-style hashes when the page has been loaded and redirects
 * to the correct address if such a hash is found.
 * This is done in JS because the hash is not sent to the server.
 */

function checkForLaineHash() {
  const RE = [
    [
      /^\#\!\/(\d{4})\/(\d\d)\/(\d\d)\/(.*)$/,
      '/$1/$2/$3/$4'
    ],
    [
      /^\#\!\/tag\/([^\/]+)$/,
      '/tag/$1'
    ],
    [
      /^\#\!\/tag\/([^\/]+)(\/\d+)$/,
      '/tag/$1/p/$2'
    ],
    [
      /^\#\!\/archives\/(\d{4})$/,
      '/archive/$1'
    ],
    [
      /^\#\!\/archives\/(\d{4})(\/\d+)$/,
      '/archive/$1/p/$2'
    ],
    [
      /^\#\!\/archives\/(\d{4})\/(\d\d)$/,
      '/archive/$1/$2'
    ],
    [
      /^\#\!\/archives\/(\d{4})\/(\d\d)(\/\d+)$/,
      '/archive/$1/$2/p/$3'
    ],
    [
      /^\#\!\/(.*)$/,
      '/$1'
    ]
  ];

  const currentHash = window.location.hash;

  if (currentHash) {
    for (const regex of RE) {
      const results = regex[0].exec(currentHash);

      if (results !== null) {
        window.location.replace(currentHash.replace(regex[0], regex[1]));
        break;
      }
    }
  }
}

export default checkForLaineHash;
