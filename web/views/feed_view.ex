defmodule MebeWeb.FeedView do
  use MebeWeb.Web, :view

  alias MebeWeb.Utils

  def show_full(), do: Utils.get_conf :feeds_full_content
end
