defmodule MebeWeb.LayoutView do
  use MebeWeb.Web, :view

  alias MebeWeb.Utils

  @doc """
  Resolve title from the current assigns. Will use the current page's or post's title, if available.
  Otherwise will render default title.
  """
  def title(conn) do
    cond do
      conn.assigns[:post] ->
        conn.assigns[:post].title

      conn.assigns[:page] ->
        conn.assigns[:page].title

      true -> Utils.get_conf :blog_name
    end
  end

  @doc """
  Return the extra HTML defined in the config.
  """
  def extra_html(), do: Utils.get_conf :extra_html

  @doc """
  Return the custom footer HTML defined in the config.
  """
  def custom_footer_html(), do: Utils.get_conf :custom_footer_html

  @doc """
  Calculate time taken for request so far.
  """
  def request_time(conn) do
    MebeWeb.RequestStartTimePlug.calculate_time conn
  end

  def safe_round(val) when is_integer(val), do: val
  def safe_round(val), do: Float.round val
end
