defmodule MebeWeb.Mixfile do
  use Mix.Project

  def project() do
    [app: :mebe_web,
     version: "1.3.2",
     elixir: "~> 1.4",
     elixirc_paths: elixirc_paths(Mix.env),
     compilers: [:phoenix] ++ Mix.compilers,
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  # Configuration for the OTP application
  #
  # Type `mix help compile.app` for more information
  def application() do
    [
      mod: {MebeWeb, []},
      extra_applications: [
        :logger
      ]
    ]
  end

  # Specifies which paths to compile per environment
  defp elixirc_paths(:test), do: ["lib", "web", "test/support"]
  defp elixirc_paths(_),     do: ["lib", "web"]

  # Specifies your project dependencies
  #
  # Type `mix help deps` for examples and options
  defp deps() do
    [
      {:phoenix, "~> 1.2.3"},
      {:phoenix_live_reload, "~> 1.0.7", only: :dev},
      {:phoenix_html, "~> 2.9.2"},
      {:cowboy, "~> 1.0.4"},
      {:earmark, "~> 1.2.0"},
      {:slugger, github: "h4cc/slugger", ref: "98e907f85d34e6c8a94270249318bdad6f838198"},
      {:calendar, "~> 0.17.2"},
      {:mbu, "~> 2.0.0"}
   ]
  end
end
