defmodule Mix.Tasks.Frontend.Watch do
  use MBU.BuildTask
  import MBU.TaskUtils
  alias Mix.Tasks.Frontend.Build.Js.Transpile, as: TranspileJS
  alias Mix.Tasks.Frontend.Build.Js.Bundle, as: BundleJS
  alias Mix.Tasks.Frontend.Build.Js.Copy, as: CopyJS
  alias Mix.Tasks.Frontend.Build.Css.Compile, as: CompileCSS
  alias Mix.Tasks.Frontend.Build.Css.Copy, as: CopyCSS

  @shortdoc "Watch frontend and rebuild when necessary"

  @deps [
    "frontend.build"
  ]

  task _ do
    [
      exec(
        TranspileJS.bin(),
        TranspileJS.args() ++ ["-w"]
      ),

      watch(
        "JSBundle",
        TranspileJS.out_path(),
        BundleJS
      ),

      watch(
        "JSCopy",
        BundleJS.out_path(),
        CopyJS
      ),

      exec(
        CompileCSS.bin(),
        CompileCSS.args() ++ [
          "-w",
          CompileCSS.scss_file()
        ]
      ),

      watch(
        "CSSCopy",
        CompileCSS.out_path(),
        CopyCSS
      )
    ]
    |> listen(watch: true)
  end
end
