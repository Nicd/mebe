defmodule Mix.Tasks.Frontend.Build.Css.Compile do
  use MBU.BuildTask
  import MebeWeb.FrontendConfs
  import MBU.TaskUtils

  @shortdoc "Build the SCSS sources"

  def bin(), do: node_bin("node-sass")

  def out_path(), do: Path.join([tmp_path(), "compiled", "css"])

  def args(), do: [
    "-o",
    out_path(),
    "--source-map",
    "true"
  ]

  def scss_file(), do: Path.join([src_path(), "css", "app.scss"])

  task _ do
    bin() |> exec(args() ++ [scss_file()]) |> listen()
  end
end
